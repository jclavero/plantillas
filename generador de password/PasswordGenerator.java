/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.azurian.azurianfitx.passgenerator;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.text.CharacterPredicates;
import org.apache.commons.text.RandomStringGenerator;

/**
 *
 * @author jclavero
 */
public class PasswordGenerator {

     private MessageDigest digester = null;
     
     
     
    public String getPassword() {

        RandomStringGenerator randomString
                = new RandomStringGenerator.Builder()
                        .withinRange('a', 'z')
                        .filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS)
                        .build();

        RandomStringGenerator randomNumbers
                = new RandomStringGenerator.Builder()
                        .withinRange('0', '9')
                        .filteredBy(CharacterPredicates.LETTERS, CharacterPredicates.DIGITS)
                        .build();

        String letras=  randomString.generate(2);
        String numeros = randomNumbers.generate(4);
        return  letras+numeros;
    }
    
     public String getMD5(String str) {
        try {
             
            digester = MessageDigest.getInstance("MD5");
            final byte[] messageDigest = digester.digest(str.getBytes());
            final BigInteger number = new BigInteger(1, messageDigest);
            // System.out.println(String.format("%032x", number).getBytes().length);
            return String.format("%032x", number);
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

}
