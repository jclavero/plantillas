/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.azurian.azurianfitx.restservices;

import cl.azurian.azurianfitx.avatar.AvatarService;
import cl.azurian.azurianfitx.entities.Paciente;
import cl.azurian.azurianfitx.jpaController.PacienteJpaController;
import cl.azurian.azurianfitx.mail.SendOne;
import cl.azurian.azurianfitx.passgenerator.PasswordGenerator;
import cl.azurian.azurianfitx.request.AgregarPacienteParams;
import cl.azurian.azurianfitx.request.DatosMetricasPacienteParams;
import cl.azurian.azurianfitx.request.EditarPacienteParams;
import cl.azurian.azurianfitx.request.NotaParams;
import cl.azurian.azurianfitx.request.PacienteFiltroResponse;
import cl.azurian.azurianfitx.request.RevisionPacienteParam;
import cl.azurian.azurianfitx.response.DatosMetricasPacienteResponse;
import cl.azurian.azurianfitx.response.DetalleGrupoPacienteResponse;
import cl.azurian.azurianfitx.response.GenericResponse;
import cl.azurian.azurianfitx.response.InfoPacienteServicesResponse;
import cl.azurian.azurianfitx.response.ListadoGruposUnPacienteResponse;
import cl.azurian.azurianfitx.response.MetricasPacienteResponse;
import cl.azurian.azurianfitx.response.TransaccionalResponse;
import java.io.IOException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jclavero
 */
@Stateless
@Path("services.paciente")
public class PacienteRestFacade {

//agregar persistence
    @PersistenceContext(unitName = "")
    private EntityManager em;
    private PacienteJpaController pacienteJpa = new PacienteJpaController();

    private final Integer PACIENTE = 0;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public GenericResponse findAll() {

        GenericResponse response = new GenericResponse();
        List<Paciente> datos = pacienteJpa.findPacienteEntities();

        response.retCode = "0";
        response.retMessage = null;
        response.retObject = datos;

        return response;
    }

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public GenericResponse findbyId(@PathParam("id") Integer id) {
        GenericResponse response = new GenericResponse();
        Paciente dato = pacienteJpa.findPaciente(id);

        response.retCode = "0";
        response.retMessage = null;
        response.retObject = dato;

        return response;
    }

    @POST
    @Path("/admin/crear")
    @Consumes({MediaType.APPLICATION_JSON})
    public GenericResponse crearPaciente(AgregarPacienteParams entity) throws IOException {

        PasswordGenerator pass = new PasswordGenerator();
        String passPaciente = pass.getPassword();
        String passMD5 = pass.getMD5(passPaciente);

        GenericResponse response = new GenericResponse();
        AvatarService imagenGenerador = new AvatarService();
        SendOne oneMail = new SendOne();

        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("agregarpaciente");
        query.setParameter("nombre_p", entity.getNombre_p());
        query.setParameter("apaterno_p", entity.getApaterno_p());
        query.setParameter("amaterno_p", entity.getAmaterno_p());
        query.setParameter("fechanacimiento_p", entity.getFechanacimiento_p());
        query.setParameter("genero_p", entity.getGenero_p());
        query.setParameter("rut_p", entity.getRut_p());
        query.setParameter("email_p", entity.getEmail_p());
        query.setParameter("digito_p", entity.getDigito_p());
        query.setParameter("avatar_p", imagenGenerador.imagenGenerator(entity.getNombre_p(), entity.getApaterno_p(), entity.getGenero_p(), PACIENTE));
        query.setParameter("contrasena_p", passMD5);
        query.setParameter("tipodocumento_p", entity.getTipodocumento_p());
        
        String nombreCompleto = entity.getNombre_p()+ " " +entity.getApaterno_p()+ " " +entity.getAmaterno_p();

        TransaccionalResponse crudResutl = (TransaccionalResponse) query.getSingleResult();

        response.retCode = crudResutl.getId();
        response.retMessage = crudResutl.getResultado();

        if ("0".equals(crudResutl.getId())) {
            oneMail.enviar(passPaciente, entity.getEmail_p(), nombreCompleto.toUpperCase());
        }

        return response;
    }

    @GET
    @Path("/detalle/{id_paciente}/{id_grupo}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response detallesPaciente(@PathParam("id_paciente") Integer id_paciente, @PathParam("id_grupo") Integer grupo_id) {
        GenericResponse response = new GenericResponse();

        try {

            response.retCode = "0";
            response.retMessage = "";

            StoredProcedureQuery query = em.createNamedStoredProcedureQuery("detallegrupopaciente");
            query.setParameter("grupo_id", grupo_id);
            query.setParameter("paciente_id", id_paciente);
            DetalleGrupoPacienteResponse detalleGrupoPaciente = (DetalleGrupoPacienteResponse) query.getSingleResult();

            StoredProcedureQuery query3 = em.createNamedStoredProcedureQuery("listadogruposunpaciente");
            query3.setParameter("paciente_id", id_paciente);
            List<ListadoGruposUnPacienteResponse> listaGruposPaciente = query3.getResultList();
                      
            InfoPacienteServicesResponse infoPaciente = new InfoPacienteServicesResponse();
            infoPaciente.setDetalleGrupoPaciente(detalleGrupoPaciente);
            infoPaciente.setListaGrupos(listaGruposPaciente);

            response.retObject = infoPaciente;

            return Response.status(Response.Status.OK).entity(response).build();

        } catch (NoResultException e) {
            response.retCode = "1";
            response.retMessage = "No existe resultado";
            System.out.println("No existe resultado: " + e);

            return Response.status(Response.Status.NOT_FOUND).entity(response).build();
        }
    }

    @POST
    @Path("/datos-metricas-paciente")
    @Produces({MediaType.APPLICATION_JSON})
    public Response dataPaciente(DatosMetricasPacienteParams entity) {
        GenericResponse response = new GenericResponse();

        System.out.println("entity.getFechaDesde(): " + entity.getFechaDesde());
        System.out.println("entity.getFechaHasta(): " + entity.getFechaHasta());

        System.out.println("DATOS DE ENTITY :___" + entity.toString());

        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("datosmetricaspaciente");
        query.setParameter("idpaciente_p", entity.getIdPaciente());
        query.setParameter("fechainicio_p", entity.getFechaDesde());
        query.setParameter("fechafin_p", entity.getFechaHasta());
        query.setParameter("ides", entity.getTiposParametros().toString().replace("[", "{").replace("]", "}"));

        System.out.println("pue ser ___" + entity.toString());

        List<DatosMetricasPacienteResponse> datosMetricasPaciente = query.getResultList();

        System.out.println("DATOS METRIAS RESPUEAS ___" + datosMetricasPaciente);

        response.retCode = "0";
        response.retMessage = "";
        response.retObject = datosMetricasPaciente;
        return Response.status(Response.Status.OK).entity(response).build();
    }

    @GET
    @Path("/parametros/{id_paciente}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response parametrosPaciente(@PathParam("id_paciente") Integer id_paciente) {
        GenericResponse response = new GenericResponse();

        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("metricaspaciente");
        query.setParameter("idpaciente_p", id_paciente);

        List<MetricasPacienteResponse> listaResp = query.getResultList();

        response.retCode = "0";
        response.retMessage = null;
        response.retObject = listaResp;

        return Response.status(Response.Status.OK).entity(response).build();

    }

    @POST
    @Path("/revision")
    @Produces({MediaType.APPLICATION_JSON})
    public Response revisar(RevisionPacienteParam entity) {

        GenericResponse response = new GenericResponse();
        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("revisionpacientes");
        query.setParameter("ides", entity.getIdes().toString().replace('[', '{').replace(']', '}'));

        List<TransaccionalResponse> respuesta = query.getResultList();

        response.retCode = "0";
        response.retObject = respuesta;

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @POST
    @Path("/nota")
    @Produces({MediaType.APPLICATION_JSON})
    public Response nota(NotaParams entity) {

        GenericResponse response = new GenericResponse();

        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("notapacientes");

        query.setParameter("ides", entity.getIdes().toString().replace('[', '{').replace(']', '}'));
        query.setParameter("nota_p", entity.getNota_p());

        List<TransaccionalResponse> resultados = query.getResultList();

        if (resultados != null && !resultados.isEmpty()) {
            response.retCode = "0";
            response.retMessage = null;
            response.retObject = resultados;

        } else {
            response.retCode = "1";
            response.retMessage = null;
            response.retObject = null;
        }
        return Response.status(Response.Status.OK).entity(response).build();
    }

    @DELETE
    @Path("/admin/{id_paciente}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public GenericResponse elimPaciente(@PathParam("id_paciente") Integer idpaciente_p) {

        GenericResponse response = new GenericResponse();
        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("eliminarpaciente");
        query.setParameter("idpaciente_p", idpaciente_p);

        TransaccionalResponse crudResutl = (TransaccionalResponse) query.getSingleResult();
        response.retCode = crudResutl.getId();
        response.retMessage = crudResutl.getResultado();
        response.retObject = null;

        return response;
    }

    @PUT
    @Path("/admin/editar")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public GenericResponse ediarPaciente(EditarPacienteParams entity) {
        GenericResponse response = new GenericResponse();

        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("editarpaciente");
        query.setParameter("idpaciente_p", entity.getIdpaciente_p());
        query.setParameter("rut_p", entity.getRut_p());
        query.setParameter("dv_p", entity.getDv_p());
        query.setParameter("nombre_p", entity.getNombre_p());
        query.setParameter("apaterno_p", entity.getApaterno_p());
        query.setParameter("amaterno_p", entity.getAmaterno_p());
        query.setParameter("email_p", entity.getEmail_p());
        query.setParameter("fechanacimiento_p", entity.getFechanacimiento_p());
        query.setParameter("genero_p", entity.getGenero_p());
        query.setParameter("tipodocumento_p", entity.getTipodocumento_p());

        TransaccionalResponse crudResutl = (TransaccionalResponse) query.getSingleResult();
        response.retCode = "0";
        response.retMessage = "";
        response.retObject = crudResutl;

        return response;
    }

    @GET
    @Path("/filtro")
    @Produces({MediaType.APPLICATION_JSON})
    public GenericResponse filtro(@QueryParam("nombre") String nombre, @QueryParam("apaterno") String apaterno, @QueryParam("amaterno") String amaterno, @QueryParam("rut") String rut, @QueryParam("dv") String dv) {

        GenericResponse response = new GenericResponse();

        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("pacienteporfiltro");
        query.setParameter("rut_p", rut);
        query.setParameter("dv_p", dv);
        query.setParameter("nombre_p", nombre);
        query.setParameter("apellidopaterno_p", apaterno);
        query.setParameter("apellidomaterno_p", amaterno);

        List<PacienteFiltroResponse> paciente = (List<PacienteFiltroResponse>) query.getResultList();
        response.retCode = "0";
        response.retMessage = "";
        response.retObject = paciente;

        return response;
    }

}
