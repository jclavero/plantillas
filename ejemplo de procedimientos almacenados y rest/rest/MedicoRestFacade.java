package cl.azurian.azurianfitx.restservices;

import cl.azurian.azurianfitx.avatar.AvatarService;
import cl.azurian.azurianfitx.entities.Medico;
import cl.azurian.azurianfitx.jpaController.MedicoJpaController;
import cl.azurian.azurianfitx.mail.SendOne;
import cl.azurian.azurianfitx.passgenerator.PasswordGenerator;
import cl.azurian.azurianfitx.request.EditarMedicoParams;
import cl.azurian.azurianfitx.request.MedicoParams;
import cl.azurian.azurianfitx.response.GenericResponse;
import cl.azurian.azurianfitx.response.TransaccionalResponse;
import java.io.IOException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jclavero
 */
@Stateless
@Path("services.medico")
public class MedicoRestFacade {

//agregar persistence
    @PersistenceContext(unitName = "")
    private EntityManager em;
    private MedicoJpaController Med = new MedicoJpaController();

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response findAll() {
        GenericResponse response = new GenericResponse();
        List<Medico> datos = Med.findMedicoEntities();

        response.retCode = "0";
        response.retMessage = null;
        response.retObject = datos;

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response findbyId(@PathParam("id") Integer id) {
        GenericResponse response = new GenericResponse();
        Medico dato = Med.findMedico(id);

        response.retCode = "0";
        response.retMessage = null;
        response.retObject = dato;

        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    @GET
    @Path("/avatar/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response datosMedico(@PathParam("id") Integer id) {
        
        GenericResponse response = new GenericResponse();
        Medico dato = Med.findMedico(id);
        
  
        
        if (dato==null) {
            response.retCode = "1";
            response.retMessage = "no tiene imagen";
            
        }else{
            response.retCode = "0";
            response.retMessage = null;
            response.retObject = dato.getAvatar();
        }
        
        
        
        return Response.status(Response.Status.OK).entity(response).build();
    }

    @POST
    @Path("/admin/crear")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response crearMedico(MedicoParams entity) throws IOException {

        PasswordGenerator pass = new PasswordGenerator();
        String passMedico = pass.getPassword();
        String passMD5 = pass.getMD5(passMedico);

        AvatarService imagenGenerador = new AvatarService();
        SendOne oneMail = new SendOne();

        GenericResponse response = new GenericResponse();

        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("agregarmedico");

        query.setParameter("idmedico_p", entity.getRutmedico());
        query.setParameter("idperfil_p", entity.getIdperfil_p());// 1 es medico - 2 es admin
        query.setParameter("nombre_p", entity.getNombre_p());
        query.setParameter("apellidopaterno", entity.getApellidopaterno());
        query.setParameter("apellidomaterno", entity.getApellidomaterno());
        query.setParameter("rutcolelgiomedico", entity.getRutcolelgiomedico());
        query.setParameter("email", entity.getEmail());
        query.setParameter("especialidad", entity.getEspecialidad());
        query.setParameter("rutmedico", entity.getRutmedico());
        query.setParameter("contrasena_p", passMD5);
        query.setParameter("dv", entity.getDv());
        
        String nombreCompleto= entity.getNombre_p()+ " " +entity.getApellidopaterno()+ " " +entity.getApellidomaterno();
        
        if (entity.getAvatar_p()==null) {
            String img = imagenGenerador.imagenGenerator(entity.getNombre_p(), entity.getApellidopaterno(), "M",entity.getIdperfil_p());
            System.out.println("URL IMAGEN: "+img);
            query.setParameter("avatar_p", img);
        } else {
            query.setParameter("avatar_p", entity.getAvatar_p());
        }

        TransaccionalResponse crudResutl = (TransaccionalResponse) query.getSingleResult();

        response.retCode = crudResutl.getId();
        response.retMessage = "Intente nuevamente";
        response.retObject = crudResutl.getResultado();
        if ("0".equals(crudResutl.getId())) {
            oneMail.enviar(passMedico, entity.getEmail(), nombreCompleto.toUpperCase());
            response.retMessage = "Se envio correo correctamente";
        }

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @DELETE
    @Path("/admin/eliminar/{idmedico_p}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response elimMedico(@PathParam("idmedico_p") Integer idmedico_p) {
        GenericResponse response = new GenericResponse();

        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("eliminarmedico");
        query.setParameter("idmedico_p", idmedico_p);

        TransaccionalResponse crudResutl = (TransaccionalResponse) query.getSingleResult();
        response.retCode = crudResutl.getId();
        response.retMessage = crudResutl.getResultado();
        response.retObject = null;

        return Response.status(Response.Status.OK).entity(response).build();
    }

    @PUT
    @Path("/admin/editar")
    @Consumes({MediaType.APPLICATION_JSON})
    public Response ediarMedico(EditarMedicoParams entity) {
        GenericResponse response = new GenericResponse();

        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("editarmedico");

        query.setParameter("idmedico_p", entity.getIdmedico_p());
        query.setParameter("idperfil_p", entity.getIdperfil_p());
        query.setParameter("mednombre_p", entity.getMednombre_p());
        query.setParameter("medapellidopaterno_p", entity.getMedapellidopaterno_p());
        query.setParameter("medapellidomaterno_p", entity.getMedapellidomaterno_p());
        query.setParameter("medrutcolelgiomedico_p", entity.getMedrutcolelgiomedico_p());
        query.setParameter("medemail_p", entity.getMedemail_p());
        query.setParameter("medespecialidad_p", entity.getMedespecialidad_p());
        query.setParameter("rutmedico_p", entity.getRutmedico_p());
        query.setParameter("digitoverificador_p", entity.getDigitoverificador_p());
        query.setParameter("avatar_p", entity.getAvatar_p());

        TransaccionalResponse crudResutl = (TransaccionalResponse) query.getSingleResult();

        response.retCode = "0";
        response.retMessage = "";
        response.retObject = crudResutl;

        return Response.status(Response.Status.OK).entity(response).build();
    }

    
    @PUT
    @Path("/admin/estado/{rut}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response estadoMedico(@PathParam("rut") String rut) {
        GenericResponse response = new GenericResponse();

        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("estadomedico");
        query.setParameter("rut", rut);

        TransaccionalResponse crudResutl = (TransaccionalResponse) query.getSingleResult();
        response.retCode = crudResutl.getId();
        response.retMessage = crudResutl.getResultado();
        response.retObject = null;

        return Response.status(Response.Status.OK).entity(response).build();
    }

}
