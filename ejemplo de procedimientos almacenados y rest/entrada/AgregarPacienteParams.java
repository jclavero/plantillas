package cl.azurian.azurianfitx.request;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author jclavero
 */
@Entity
public class AgregarPacienteParams implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String nombre_p;
    private String apaterno_p;
    private String amaterno_p;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechanacimiento_p;
    
    private String genero_p;
    private String rut_p;
    private String email_p;
    private String digito_p;
    private String contrasena_p;
    private String avatar_p;
    private int tipodocumento_p;

    public String getNombre_p() {
        return nombre_p;
    }

    public void setNombre_p(String nombre_p) {
        this.nombre_p = nombre_p;
    }

    public String getApaterno_p() {
        return apaterno_p;
    }

    public void setApaterno_p(String apaterno_p) {
        this.apaterno_p = apaterno_p;
    }

    public String getAmaterno_p() {
        return amaterno_p;
    }

    public void setAmaterno_p(String amaterno_p) {
        this.amaterno_p = amaterno_p;
    }

    public Date getFechanacimiento_p() {
        return fechanacimiento_p;
    }

    public void setFechanacimiento_p(Date fechanacimiento_p) {
        this.fechanacimiento_p = fechanacimiento_p;
    }

    public String getGenero_p() {
        return genero_p;
    }

    public void setGenero_p(String genero_p) {
        this.genero_p = genero_p;
    }

    public String getRut_p() {
        return rut_p;
    }

    public void setRut_p(String rut_p) {
        this.rut_p = rut_p;
    }

    public String getEmail_p() {
        return email_p;
    }

    public void setEmail_p(String email_p) {
        this.email_p = email_p;
    }

    public String getDigito_p() {
        return digito_p;
    }

    public void setDigito_p(String digito_p) {
        this.digito_p = digito_p;
    }

    public String getAvatar_p() {
        return avatar_p;
    }

    public void setAvatar_p(String avatar_p) {
        this.avatar_p = avatar_p;
    }

    public String getContrasena_p() {
        return contrasena_p;
    }

    public void setContrasena_p(String contrasena_p) {
        this.contrasena_p = contrasena_p;
    }

    public int getTipodocumento_p() {
        return tipodocumento_p;
    }

    public void setTipodocumento_p(int tipodocumento_p) {
        this.tipodocumento_p = tipodocumento_p;
    }

    
    
    

    

}
