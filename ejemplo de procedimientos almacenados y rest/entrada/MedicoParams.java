/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.azurian.azurianfitx.request;


import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jclavero
 */
@Entity

public class MedicoParams implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idmedico")
    private Long idmedico_p;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idperfil")
    private int idperfil_p;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "mednombre")
    private String nombre_p;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "medapellidopaterno")
    private String apellidopaterno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "medapellidomaterno")
    private String apellidomaterno;
    @Size(max = 50)
    @Column(name = "medrutcolelgiomedico")
    private String rutcolelgiomedico;
    @Size(max = 100)
    @Column(name = "medemail")
    private String email;
    @Size(max = 100)
    @Column(name = "medespecialidad")
    private String especialidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "rutmedico")
    private long rutmedico;

    @Column(name = "digitoverificador")
    private String dv;
    
    @Column(name = "contraseña")
    private String contrasena_p;
    
     @Column(name = "avatar_p")
    private String avatar_p;

    public String getAvatar_p() {
        return avatar_p;
    }

    public void setAvatar_p(String avatar_p) {
        this.avatar_p = avatar_p;
    }


     
    public Long getIdmedico_p() {
        return idmedico_p;
    }

    public void setIdmedico_p(Long idmedico_p) {
        this.idmedico_p = idmedico_p;
    }

    public int getIdperfil_p() {
        return idperfil_p;
    }

    public void setIdperfil_p(int idperfil_p) {
        this.idperfil_p = idperfil_p;
    }

    public String getNombre_p() {
        return nombre_p;
    }

    public void setNombre_p(String nombre_p) {
        this.nombre_p = nombre_p;
    }

    public String getApellidopaterno() {
        return apellidopaterno;
    }

    public void setApellidopaterno(String apellidopaterno) {
        this.apellidopaterno = apellidopaterno;
    }

    public String getApellidomaterno() {
        return apellidomaterno;
    }

    public void setApellidomaterno(String apellidomaterno) {
        this.apellidomaterno = apellidomaterno;
    }


    public String getRutcolelgiomedico() {
        return rutcolelgiomedico;
    }

    public void setRutcolelgiomedico(String rutcolelgiomedico) {
        this.rutcolelgiomedico = rutcolelgiomedico;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public long getRutmedico() {
        return rutmedico;
    }

    public void setRutmedico(long rutmedico) {
        this.rutmedico = rutmedico;
    }

    public String getDv() {
        return dv;
    }

    public void setDv(String dv) {
        this.dv = dv;
    }

    public String getContrasena_p() {
        return contrasena_p;
    }

    public void setContrasena_p(String contrasena_p) {
        this.contrasena_p = contrasena_p;
    }    

    @Override
    public String toString() {
        return "MedicoParams{" + "idmedico_p=" + idmedico_p + ", idperfil_p=" + idperfil_p + ", nombre_p=" + nombre_p + ", apellidopaterno=" + apellidopaterno + ", apellidomaterno=" + apellidomaterno + ", rutcolelgiomedico=" + rutcolelgiomedico + ", email=" + email + ", especialidad=" + especialidad + ", rutmedico=" + rutmedico + ", dv=" + dv + ", contrasena_p=" + contrasena_p + '}';
    }
    
    
    
}
