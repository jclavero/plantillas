/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */package cl.azurian.azurianfitx.response;

import java.util.List;


/**
 *
 * @author jclavero
 */
public class InfoPacienteServicesResponse{

    
    private DetalleGrupoPacienteResponse detalleGrupoPaciente;
    private List<ListadoGruposUnPacienteResponse> listaGrupos;

    public DetalleGrupoPacienteResponse getDetalleGrupoPaciente() {
        return detalleGrupoPaciente;
    }

    public void setDetalleGrupoPaciente(DetalleGrupoPacienteResponse detalleGrupoPaciente) {
        this.detalleGrupoPaciente = detalleGrupoPaciente;
    }

    public List<ListadoGruposUnPacienteResponse> getListaGrupos() {
        return listaGrupos;
    }

    public void setListaGrupos(List<ListadoGruposUnPacienteResponse> listaGrupos) {
        this.listaGrupos = listaGrupos;
    }
  
}
