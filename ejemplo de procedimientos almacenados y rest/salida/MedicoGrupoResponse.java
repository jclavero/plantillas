/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.azurian.azurianfitx.response;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

/**
 *
 * @author jclavero
 */
@Entity
 @NamedStoredProcedureQuery(
            name = "listamedicogrupo",
            procedureName = "listamedicogrupo",
            resultClasses = {MedicoGrupoResponse.class},
            parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, type = Integer.class, name = "idgrupo_p")
            }
    )
public class MedicoGrupoResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer idmedico;
    String mednombre;
    Long rutmedico;
    String medapellidopaterno;
    String digitoverificador;
    String medemail;
    String medapellidomaterno;
    boolean estado;

    public Integer getIdmedico() {
        return idmedico;
    }

    public void setIdmedico(Integer idmedico) {
        this.idmedico = idmedico;
    }

    public String getMednombre() {
        return mednombre;
    }

    public void setMednombre(String mednombre) {
        this.mednombre = mednombre;
    }

    public Long getRutmedico() {
        return rutmedico;
    }

    public void setRutmedico(Long rutmedico) {
        this.rutmedico = rutmedico;
    }

    public String getMedapellidopaterno() {
        return medapellidopaterno;
    }

    public void setMedapellidopaterno(String medapellidopaterno) {
        this.medapellidopaterno = medapellidopaterno;
    }

    public String getDigitoverificador() {
        return digitoverificador;
    }

    public void setDigitoverificador(String digitoverificador) {
        this.digitoverificador = digitoverificador;
    }

    public String getMedemail() {
        return medemail;
    }

    public void setMedemail(String medemail) {
        this.medemail = medemail;
    }

    public String getMedapellidomaterno() {
        return medapellidomaterno;
    }

    public void setMedapellidomaterno(String medapellidomaterno) {
        this.medapellidomaterno = medapellidomaterno;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    

}
