/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import org.json.JSONException;

/**
 *
 * @author jclavero
 */
public class Utils {

    private String[] medico = {
        "mednombre",
        "idmedico",
        "medapellidopaterno",
        "medapellidomaterno",
        "medrutcolelgiomedico",
        "medemail",
        "medespecialidad",
        "rutmedico",
        "estado",
        "digitoverificador",
        "admin"};
    private String[] paciente = {
        "pacnombre",
        "idpaciente",
        "pacapellidopaterno",
        "pacapellidomaterno",
        "pacedad",
        "pacgenero",
        "pacrut",
        "pemail",
        "estado",
        "digitoverificador"};

    public String cortadorBloquesMedicoResponse(byte[] decrypted) throws JSONException, JsonProcessingException {
        int aux = 0;
        String json = "";
        json += "{";
        for (int i = 32; i < (decrypted.length - 32); i = i + 32) {
            byte[] bloque = new byte[32];
            System.arraycopy(decrypted, i, bloque, 0, bloque.length);
            String value = new String(bloque, StandardCharsets.ISO_8859_1);
            value = value.trim();

            String key = "\"" + medico[aux] + "\"";
            aux++;
            if (key.equals("\"admin\"") || key.equals("\"estado\"") || key.equals("\"idmedico\"")) {

                json += key + ":" + value;
            } else {
                json += key + ":" + "\"" + value + "\"";
            }
            json.lastIndexOf("\"");
            if (i != 352) {

                json += ",";
            }

        }
        json += "}";
        return json;
    }

    public String cortadorBloquesPacienteResponse(byte[] decrypted) throws JSONException, JsonProcessingException, UnsupportedEncodingException {
        int aux = 0;
        String json = "";
        json += "{";

        for (int i = 32; i < (decrypted.length - 32); i = i + 32) {
            byte[] bloque = new byte[32];
            System.arraycopy(decrypted, i, bloque, 0, bloque.length);
            String value = new String(bloque, StandardCharsets.ISO_8859_1);
            value = value.trim();
            String key = "\"" + paciente[aux] + "\"";
            aux++;
            if (key.equals("\"estado\"") || key.equals("\"idpaciente\"")) {

                json += key + ":" + value;
            } else {
                json += key + ":" + "\"" + value + "\"";
            }
            json.lastIndexOf("\"");
            if (i != 320) {

                json += ",";
            }
        }
        json += "}";
        return json;
    }

    public byte[] cortadorBloquesMedico(byte[] decrypted) throws JSONException {
        byte[] bloque = new byte[352];
        System.arraycopy(decrypted, 32, bloque, 0, (decrypted.length) - 64);
        return bloque;
    }

    public byte[] cortadorBloquesPaciente(byte[] decrypted) throws JSONException {
        byte[] bloque = new byte[320];
        System.arraycopy(decrypted, 32, bloque, 0, (decrypted.length) - 64);
        return bloque;
    }

}
