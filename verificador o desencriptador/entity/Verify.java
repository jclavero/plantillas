
package entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "tokenMedico",
    "tokenPaciente"
})
public class Verify implements Serializable
{

    @JsonProperty("tokenMedico")
    private String tokenMedico;
    @JsonProperty("tokenPaciente")
    private String tokenPaciente;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -626430229020954333L;

    @JsonProperty("tokenMedico")
    public String getTokenMedico() {
        return tokenMedico;
    }

    @JsonProperty("tokenMedico")
    public void setTokenMedico(String tokenMedico) {
        this.tokenMedico = tokenMedico;
    }

    @JsonProperty("tokenPaciente")
    public String getTokenPaciente() {
        return tokenPaciente;
    }

    @JsonProperty("tokenPaciente")
    public void setTokenPaciente(String tokenPaciente) {
        this.tokenPaciente = tokenPaciente;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
