/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.azurian.verify;

import controller.VerifyRest;
import filtros.SharingFilter;
import java.util.Set;
import javax.ws.rs.core.Application;
/**
 *
 * @author jclavero
 */
@javax.ws.rs.ApplicationPath("/services")
public class JEEApplication extends Application{
  
    
    @Override
    public Set<Class<?>> getClasses() {
        //return super.getClasses(); //To change body of generated methods, choose Tools | Templates.
        Set<Class<?>> recursos = new java.util.HashSet<>();
        recursos.add(VerifyRest.class);
        recursos.add(SharingFilter.class);
        //recursos.add(com.azurian.zauthmedicos.LoginRest.class);
       return recursos; 
    }
  
  
    
    
}
