/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import entity.ErrorHandler;

import entity.Verify;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import service.Utils;
import service.VerifyService;

/**
 *
 * @author jclavero
 */
@Stateless
@Path("verify")
public class VerifyRest {

    ErrorHandler error = new ErrorHandler();
    VerifyService verifyService = new VerifyService();
    Utils util = new Utils();
    String keyAES = "keyPrueba";

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response doLogin(Verify entity) throws Exception {
        Response resp = null;
        try {
   
            
            error.setMessageError("Error");
            error.setErrorCode(400);

            if (entity.getTokenMedico() != null) {

                String jsonSecurityMedico = entity.getTokenMedico();
                byte[] decryp = verifyService.decryptAES1(jsonSecurityMedico, keyAES);

                byte[] jsonMedico = util.cortadorBloquesMedico(decryp);
                String responseJson = util.cortadorBloquesMedicoResponse(decryp);
                if (verifyService.controladorOperacion(jsonMedico, jsonSecurityMedico)) {
                    resp = Response.accepted(responseJson).build();
                } else {
                    resp = Response.status(Response.Status.BAD_REQUEST).entity(error).build();
                }
            } else {

                String jsonSecurityPaciente = entity.getTokenPaciente();
                byte[] decryp = verifyService.decryptAES1(jsonSecurityPaciente, keyAES);

                byte[] jsonPaciente = util.cortadorBloquesPaciente(decryp);
                String responseJson = util.cortadorBloquesPacienteResponse(decryp);
                if (verifyService.controladorOperacion(jsonPaciente, jsonSecurityPaciente)) {
                    resp = Response.accepted(responseJson).build();
                } else {
                    resp = Response.status(Response.Status.BAD_REQUEST).entity(error).build();
                }
            }

        } catch (Exception ex) {

            return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
        }
        return resp;

    }

}
