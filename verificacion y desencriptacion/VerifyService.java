package cl.azurian.azurianfitx.verificacion;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class VerifyService {

    private MessageDigest digester = null;

    private String getMD5(String str) {
        try {
            digester = MessageDigest.getInstance("MD5");
            final byte[] messageDigest = digester.digest(str.getBytes());
            final BigInteger number = new BigInteger(1, messageDigest);
            // System.out.println(String.format("%032x", number).getBytes().length);
            return String.format("%032x", number);
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    public byte[] decryptAES1(String value) {
        try {
            String keyAES = "Hola1234Hola1234";
            byte[] encrypted = Base64.getDecoder().decode(value);
            SecretKeySpec secretKey = new SecretKeySpec(keyAES.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] decrypted = cipher.doFinal(encrypted);
            return decrypted;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private String getMD5Encriptado(byte[] decrypted) {
        byte[] md5 = new byte[32];
        System.arraycopy(decrypted, 0, md5, 0, md5.length);
        String md5String = new String(md5, StandardCharsets.UTF_8);
        md5String.trim();
        return md5String;
    }

    public boolean controladorOperacion(byte[] jsonCompleto, String jsonEncriptado) {
        System.out.println(jsonCompleto);
        jsonEncriptado = jsonEncriptado.replaceAll("\"", "");
       
        String md5Completo = getMD5(Arrays.toString(jsonCompleto));
        byte[] md5b = decryptAES1(jsonEncriptado);
        String md5Encriptado = getMD5Encriptado(md5b);
        if (md5Completo.equals(md5Encriptado)) {
            return true;
        } else {
            return false;
        }
    }

}
