/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.azurian.azurianfitx.mail;

import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.SendGrid;
import java.io.IOException;
import java.util.ResourceBundle;

/**
 *
 * @author jclavero
 */
public class SendOne {

    public void enviar(String pass, String email, String nombre) throws IOException {

        //  try{
        String apiKey = ResourceBundle.getBundle("/Bundle").getString("keyOfSengrid");
        //esta apikey?    
        SendGrid sg = new SendGrid(apiKey);
        Email from = new Email("test@example.com");
        
        String subject = "Confirmación contraseña";
        
        Email to = new Email(email);
        com.sendgrid.Content content = new com.sendgrid.Content("text/html", "<!DOCTYPE html> <html> <head> <title></title> <meta charset=\"utf-8\"> <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" /> <style type=\"text/css\"> /* CLIENT-SPECIFIC STYLES */ body, table, td, a{-webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;} /* Prevent WebKit and Windows mobile changing default text sizes */ table, td{mso-table-lspace: 0pt; mso-table-rspace: 0pt;} /* Remove spacing between tables in Outlook 2007 and up */ img{-ms-interpolation-mode: bicubic;} /* Allow smoother rendering of resized image in Internet Explorer */  /* RESET STYLES */ img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none;} table{border-collapse: collapse !important;} body{height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important;}  /* iOS BLUE LINKS */ a[x-apple-data-detectors] { color: inherit !important; text-decoration: none !important; font-size: inherit !important; font-family: inherit !important; font-weight: inherit !important; line-height: inherit !important; }  /* MOBILE STYLES */ @media screen and (max-width: 525px) {  /* ALLOWS FOR FLUID TABLES */ .wrapper { width: 100% !important; max-width: 100% !important; }  /* ADJUSTS LAYOUT OF LOGO IMAGE */ .logo img { margin: 0 auto !important; }  /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */ .mobile-hide { display: none !important; }  .img-max { max-width: 100% !important; width: 100% !important; height: auto !important; }  /* FULL-WIDTH TABLES */ .responsive-table { width: 100% !important; }  /* UTILITY CLASSES FOR ADJUSTING PADDING ON MOBILE */ .padding { padding: 10px 5% 15px 5% !important; }  .padding-meta { padding: 30px 5% 0px 5% !important; text-align: center; }  .padding-copy { padding: 10px 5% 10px 5% !important; text-align: center; }  .no-padding { padding: 0 !important; }  .section-padding { padding: 50px 15px 50px 15px !important; }  /* ADJUST BUTTONS ON MOBILE */ .mobile-button-container { margin: 0 auto; width: 100% !important; }  .mobile-button { padding: 15px !important; border: 0 !important; font-size: 16px !important; display: block !important; }  }  /* ANDROID CENTER FIX */ div[style*=\"margin: 16px 0;\"] { margin: 0 !important; } </style> </head> <body style=\"margin: 0 !important; padding: 0 !important;\"> <!-- HIDDEN PREHEADER TEXT --> <div style=\"display: none; font-size: 1px; color: #fefefe; line-height: 1px; font-family: Helvetica, Arial, sans-serif; max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden;\"> Mensaje Generico de Clinica Río Blanco </div> <!-- HEADER --> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"> <tr> <td bgcolor=\"#ffffff\" align=\"center\"> <!--[if (gte mso 9)|(IE)]> <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\"> <tr> <td align=\"center\" valign=\"top\" width=\"500\"> <![endif]--> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"wrapper\"> <tr> <td align=\"center\" valign=\"top\" style=\"padding: 15px 0;\" class=\"logo\"> <a href=\"http://clinicarioblanco.cl\" target=\"_blank\"> <img alt=\"Logo\" src=\"http://www.clinicarioblanco.cl/wp-content/uploads/2017/12/logo_CRB_7.png\" style=\"display: block; font-family: Helvetica, Arial, sans-serif; color: #ffffff; font-size: 16px;\" border=\"0\"> </a> </td> </tr> </table> <!--[if (gte mso 9)|(IE)]> </td> </tr> </table> <![endif]--> </td> </tr> <tr> <td bgcolor=\"#D8F1FF\" align=\"center\" style=\"padding: 10px 15px 70px 15px;\" class=\"section-padding\"> <!--[if (gte mso 9)|(IE)]> <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\"> <tr> <td align=\"center\" valign=\"top\" width=\"500\"> <![endif]--> <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" style=\"max-width: 500px;\" class=\"responsive-table\"> <tr> <td> <!-- HERO IMAGE --> <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tr> <td style=\"font-size: 25px; font-family: Helvetica, Arial, sans-serif; color: #333333; padding-top: 30px;\" class=\"padding\">Estimado(a): </br>" +nombre+ "</td>  </tr> <tr> <td style=\"padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\" class=\"padding\">Queremos informale lo siguiente: su contraseña es : " + pass + "</td> </tr> <tr style=\"padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\"><td> </br> </br> </td></tr> <tr style=\"padding: 20px 0 0 0; font-size: 16px; line-height: 25px; font-family: Helvetica, Arial, sans-serif; color: #666666;\"><td>Sin otro particular, se despide.</br>Atentamente.</td> </tr> <tr> </tr> </table> </td> </tr> </table> <!--[if (gte mso 9)|(IE)]> </td> </tr> </table> <![endif]--> </td> </tr> <tr> <td bgcolor=\"#ffffff\" align=\"center\" style=\"padding: 20px 0px;\"> <!--[if (gte mso 9)|(IE)]> <table align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"500\"> <tr> <td align=\"center\" valign=\"top\" width=\"500\"> <![endif]--> <!-- UNSUBSCRIBE COPY --> <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\" style=\"max-width: 500px;\" class=\"responsive-table\"> <tr> <td align=\"center\" style=\"font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#666666;\"> Av. Santa María 777, Villa Minera Andina, Los Andes, V Región, Chile. </td> </tr> </table> <!--[if (gte mso 9)|(IE)]> </td> </tr> </table> <![endif]--> </td> </tr> </table> </body> </html>");
        Mail mail = new Mail(from, subject, to, content);
        //SendGrid sg = new SendGrid(System.getenv("SENDGRID_API_KEY"));
        Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            sg.api(request);

        } catch (IOException ex) {
            throw ex;
        }

    }

}
