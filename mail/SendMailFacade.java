/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.azurian.azurianfitx.mail;

import cl.azurian.azurianfitx.response.GenericResponse;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import java.io.IOException;
import java.util.List;
import java.util.ResourceBundle;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author jclavero
 */
@Stateless
@Path("services.mail")
public class SendMailFacade {

    @POST
    @Path("/enviar")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public GenericResponse enviar(MailEntity entity) throws IOException {

        GenericResponse resp = new GenericResponse();

          try{
        String apiKey = ResourceBundle.getBundle("/Bundle").getString("keyOfSengrid");
        //esta apikey?
        System.out.println("! apiKey " + apiKey);
        SendGrid sg = new SendGrid(apiKey);

            List<To> listaTo        = entity.getPersonalizations().get(0).getTo();
           // List<Email> listaTo        = entity.getPersonalizations().get(0).getTos();
            String subject          = entity.getPersonalizations().get(0).getSubject();
            String contentVal       =  entity.getContent().get(0).getValue();
            String contentTyp       = entity.getContent().get(0).getType();
            System.out.println("! el  subject es "+subject);
            System.out.println("! el typo es "+contentTyp);
            System.out.println("! el VAL es "+contentVal);
                    
            for(To t : listaTo){
                //A quien enviamos?
                System.out.println("! Enviamos a "+t.getEmail());
                Email toM = new Email(t.getEmail());

                Content cont = new Content(contentTyp, contentVal);
                System.out.println("! Contenido "+cont.toString());
                Mail mail = new Mail(new Email("no-reply@test.cl"),subject,toM,cont);
                
                System.out.println(mail.buildPretty());
                
                System.out.println("! Mail "+mail.toString());
                Request request = new Request();
                System.out.println("! Request "+request.toString());
                request.setMethod(Method.POST);
                request.setEndpoint("mail/send");
                request.setBody(mail.build());
                System.out.println("! body "+mail.buildPretty());
                 Response response = sg.api(request);
                System.out.println("! Response "+response.getBody());
            }
    
        }catch(Exception e){
            resp.retCode="1";
            resp.retMessage = "Error Envio Correo";
            resp.retObject=e.getMessage();
            
        }
          resp.retCode="0";
            resp.retMessage = "El Envío De Correos se Realizó Correctamente";
            
        return resp;
    }

}