/**
*
*servicio para generar una imagen con las iniciales un nombre
*/
package cl.azurian.azurianfitx.avatar;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import javax.imageio.ImageIO;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

/**
 *
 * @author jclavero
 */
public class AvatarService {


    public String imagenGenerator(String inicialNombre, String inicialApellido, String genero, Integer id_perfil) throws IOException {

 
        String nombre = inicialNombre.substring(0,1).toUpperCase();
        String apellido = inicialApellido.substring(0,1).toUpperCase();
        
        
        
        int ancho = 250;
        int alto = 250;

        RenderingHints rh = new RenderingHints(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        BufferedImage bufferedImage = new BufferedImage(ancho, alto, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = bufferedImage.createGraphics();
        g2d.setRenderingHints(rh);

        if (genero.equalsIgnoreCase("M")) {
            //Color Masculino
            Color masculino = new Color(0, 153, 255);
            g2d.setColor(masculino);
            g2d.fillRect(0, 0, ancho, alto);
        } else {
            //Color Femenino
            Color femenino = new Color(255, 0, 102);
            g2d.setColor(femenino);
            g2d.fillRect(0, 0, ancho, alto);
        }

        g2d.setColor(Color.white);
        int estilo = Font.LAYOUT_LEFT_TO_RIGHT;
        Font fuente = new Font("Arial", estilo, 120);
        g2d.setFont(fuente);
        g2d.drawString(nombre + apellido, 40, 155);

        g2d.dispose();
      
        ByteArrayOutputStream b=new ByteArrayOutputStream();
        ImageIO.write(bufferedImage, "png", b);
        byte[] img=b.toByteArray();
    
        
        Random random = new Random();
        int randomVal = random.nextInt();
        Date fecha = new Date();
        String nombreArchivo = randomVal + "_" + fecha.getTime();
       
         UploadServiceAws aws = UploadServiceAws.getInstance();
        String respuestaAws = aws.subirImagen(nombreArchivo,img);
        System.out.println("ARCHIVO SUBIDO CON ESTA RUTA : " + respuestaAws);

       

//        //Response
//        UrlResult2 result = new UrlResult2();
//        //String nameRuta = savePath + pathlocali + nombreArchivo + ".jpg";
//        result.setUrl(respuestaAws);
//        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
//        String JSON = ow.writeValueAsString(result);
//      

        return respuestaAws;
    }
 class UrlResult2 {

        String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String toString() {
            return "urlResult [url=" + url + "]";
        }

    }
}
