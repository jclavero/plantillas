
//servlet para subir una imagen 

package cl.azurian.azurianfitx.avatar;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;

/**
 * @author jclavero
 */
@WebServlet("/upload")
@MultipartConfig
public class UploadServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private String prefix = null;
    private String savePath = null;
    private String pathlocali = null;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * @see Servlet#init(ServletConfig)
     */
    public void init(ServletConfig config) throws ServletException {
        ServletContext c = config.getServletContext();
        prefix = c.getInitParameter("prefix");
        savePath = c.getInitParameter("savePath");
        pathlocali = c.getInitParameter("pathlocali");
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        response.getWriter().append("Served at: ").append(request.getContextPath());

        throw new ServletException("GET method used with " + getClass().getName() + ": POST method required.");
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doOptions(req, resp); //To change body of generated methods, choose Tools | Templates.
        System.out.println("cl.azurian.azurianfitx.avatar.UploadServlet.doOptions()");
        resp.setHeader("Access-Control-Allow-Origin", "*");
        resp.setHeader("Access-Control-Allow-Methods", "POST");
        resp.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
        resp.setStatus(HttpServletResponse.SC_OK);

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("cl.azurian.azurianfitx.avatar.UploadServlet.doPost()");
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
        response.addHeader("Access-Control-Allow-Credentials", "*");

        response.addHeader("Access-Control-Allow-Methods", "POST");
        Part filePart = request.getPart("file");

        InputStream inputStream = filePart.getInputStream();

        byte[] buffer = new byte[inputStream.available()];
        inputStream.read(buffer);
        //TODO: Eliminar cuando este listo servicio de Avatar
        Random random = new Random();
        int randomVal = random.nextInt();
        Date fecha = new Date();
        String nombreArchivo = randomVal + "_" + fecha.getTime();
        //convertirByteImg(buffer, nombreArchivo);

        UploadServiceAws aws = UploadServiceAws.getInstance();
        String respuestaAws = aws.subirImagen(nombreArchivo, buffer);
        System.out.println("ARCHIVO SUBIDO CON ESTA RUTA : " + respuestaAws);

        response.setContentType("application/json;charset=UTF-8");

        //Response
        UrlResult result = new UrlResult();
        //String nameRuta = savePath + pathlocali + nombreArchivo + ".jpg";
        result.setUrl(respuestaAws);
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String JSON = ow.writeValueAsString(result);
        java.io.PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(JSON);
        out.flush();

    }

    private void convertirByteImg(byte[] bytes, String nombreArchivo) {

        try {

            if (bytes != null && bytes.length > 0) {
                ByteArrayInputStream bis = new ByteArrayInputStream(bytes);

                //File f = new File(ruta);
                //f.write(bytes);
                BufferedImage image = ImageIO.read(bis);
                System.out.println("image::" + image);
                String url = prefix + pathlocali + nombreArchivo + ".jpg";
                System.out.println("url::" + url);
                ImageIO.write(image, "jpg", new File(url));
                System.out.println("image created");
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    //Class para retorno
    class UrlResult {

        String url;

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String toString() {
            return "urlResult [url=" + url + "]";
        }

    }
}
