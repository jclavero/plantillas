/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acces.repository;

import com.acces.entity.Acces;
import com.acces.repository.exceptions.NonexistentEntityException;
import com.acces.repository.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author jclavero
 */
public class AccesJpaController implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AccesJpaController() {
        this.emf = Persistence.createEntityManagerFactory("com_pruebaforgot_jar_1.0-SNAPSHOTPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Acces acces) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(acces);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findAcces(acces.getToken()) != null) {
                throw new PreexistingEntityException("Acces " + acces + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Acces acces) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            acces = em.merge(acces);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = acces.getToken();
                if (findAcces(id) == null) {
                    throw new NonexistentEntityException("The acces with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Acces acces;
            try {
                acces = em.getReference(Acces.class, id);
                acces.getToken();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The acces with id " + id + " no longer exists.", enfe);
            }
            em.remove(acces);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Acces> findAccesEntities() {
        return findAccesEntities(true, -1, -1);
    }

    public List<Acces> findAccesEntities(int maxResults, int firstResult) {
        return findAccesEntities(false, maxResults, firstResult);
    }

    private List<Acces> findAccesEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Acces.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Acces findAcces(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Acces.class, id);
        } finally {
            em.close();
        }
    }

    public int getAccesCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Acces> rt = cq.from(Acces.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public static void main(String[] args){
        
    AccesJpaController service = new AccesJpaController();
    
    Acces aux = service.findAcces("123asdd123");
        
//        Acces entity = new Acces();
//        entity.setMail("clavero.deoliveira@gmail.com");
//        entity.setToken("123asdd123");
//        entity.setExpires(10);
//        
//        try {
//            service.create(entity);
          System.out.println("RESP________________"+aux.toString());
//       } catch (Exception ex) {
//            Logger.getLogger(AccesJpaController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        
    }
//    
}
