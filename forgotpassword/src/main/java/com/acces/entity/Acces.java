/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acces.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author jclavero
 */
@Entity
@Table(name = "ACCES", schema="DTERECEPPRODOS")
@NamedQueries({
    @NamedQuery(name = "Acces.findAll", query = "SELECT a FROM Acces a")
    , @NamedQuery(name = "Acces.findByToken", query = "SELECT a FROM Acces a WHERE a.token = :token")
    , @NamedQuery(name = "Acces.findByMail", query = "SELECT a FROM Acces a WHERE a.mail = :mail")
    , @NamedQuery(name = "Acces.findByExpires", query = "SELECT a FROM Acces a WHERE a.expires = :expires")})
public class Acces implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "token")
    private String token;
    @Column(name = "mail")
    private String mail;
    @Column(name = "expires")
    @Temporal(TemporalType.DATE)
    private Date expires;

    public Acces() {
    }

    public Acces(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public Date getExpires() {
        return expires;
    }

    public void setExpires(Date expires) {
        this.expires = expires;
    }

    public void setExpires(int minutes){
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, minutes);
        this.expires = now.getTime();
    }

    public boolean isExpired() {
        return new Date().after(this.expires);
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (token != null ? token.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Acces)) {
            return false;
        }
        Acces other = (Acces) object;
        if ((this.token == null && other.token != null) || (this.token != null && !this.token.equals(other.token))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.acces.entity.Acces[ token=" + token + " ]";
    }
    
}
