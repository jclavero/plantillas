/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.azurian.azurianfitx.mensajeriaPush;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;

/**
 *
 * @author jclavero
 */
@Entity
 @NamedStoredProcedureQuery(
            name = "listapacientesmobile",
            procedureName = "listapacientesmobile",
            resultClasses = {ResponsePush.class},
            parameters = {
                @StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "idpacientes_p")
            }
      )
public class ResponsePush implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer pacientes;
    private String token;
    private String mobile;

    public Integer getPacientes() {
        return pacientes;
    }

    public void setPacientes(Integer pacientes) {
        this.pacientes = pacientes;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    
}
