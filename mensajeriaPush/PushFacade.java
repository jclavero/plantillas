/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.azurian.azurianfitx.mensajeriaPush;

import cl.azurian.azurianfitx.response.BuscarMensajeResponse;
import cl.azurian.azurianfitx.response.GenericResponse;
import cl.azurian.azurianfitx.response.TransaccionalResponse;
import cl.azurian.azurianfitx.restservices.AdmPasswordRestFacade;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jclavero
 */
@Stateless
@Path("api")
public class PushFacade {

    //@PersistenceContext(unitName = "cl.azurian_AzurianFitX_war_1PU")
    //private EntityManager em;

    @POST
    @Path("/push")
    @Produces({MediaType.APPLICATION_JSON})
    public Response prueba2(RequesParams entity) throws Exception {

        try{
        GenericResponse response = new GenericResponse();
        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("listapacientesmobile");
        query.setParameter("idpacientes_p", entity.getIdpacientes_p().toString().replace('[', '{').replace(']', '}'));
        
        
        List<ResponsePush> crudResutl = query.getResultList();

      
        
        for (ResponsePush e : crudResutl) {
            if (!e.getMobile().equalsIgnoreCase("android")) {

                PushIOS ios = new PushIOS();

                ios.enviar(crudResutl.get(0).getToken(), entity.getMensaje_p());
                
                System.out.println("QUE ES GET 0 ___:" +crudResutl.get(0).getToken());

            } else {

                PushAndroid andorid = new PushAndroid();

                andorid.pushFCMNotification(e.getToken(), entity.getTitulo(), entity.getMensaje_p());
            }
        }
          StoredProcedureQuery query2 = em.createNamedStoredProcedureQuery("insertarmensaje");
        query2.setParameter("idmedico_p", entity.getIdmedico_p());
        query2.setParameter("idpacientes_p", entity.getIdpacientes_p().toString().replace('[', '{').replace(']', '}'));
        query2.setParameter("mensaje_p", entity.getMensaje_p());

        List<TransaccionalResponse> crudResult2 = (List<TransaccionalResponse>) query2.getResultList();
          response.retCode = crudResult2.get(0).getId();
          response.retMessage = crudResult2.get(0).getResultado();
        response.retObject = crudResult2;
        
        

        return Response.accepted().entity(response).build();
        }catch(Exception ex){
         Logger.getLogger(AdmPasswordRestFacade.class.getName()).log(Level.SEVERE, null, ex);  
        }
        return Response.status(Response.Status.NOT_FOUND).build();
    }
    
    
    @GET
    @Path("/{medico_id}/{paciente_id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response mensajes(@PathParam("medico_id") Integer medico_id, @PathParam("paciente_id") Integer paciente_id) {
        GenericResponse response = new GenericResponse();
        
        StoredProcedureQuery query = em.createNamedStoredProcedureQuery("buscarmensaje");
        query.setParameter("idmedico_p", paciente_id);
        query.setParameter("idpaciente_p", medico_id);
        
         List<BuscarMensajeResponse> datos =  query.getResultList();

        response.retCode = "0";
        response.retMessage = null;
        response.retObject = datos;

        return Response.status(Response.Status.OK).entity(response).build();
    }
    
    
    
    
    
    
    
    
}
