/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.azurian.azurianfitx.mensajeriaPush;


import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jclavero
 */

public class RequesParams implements Serializable {

    private Integer idmedico_p;
    private List<Long> idpacientes_p;
    private String titulo;
    private String  mensaje_p;

    public Integer getIdmedico_p() {
        return idmedico_p;
    }

    public void setIdmedico_p(Integer idmedico_p) {
        this.idmedico_p = idmedico_p;
    }

    public List<Long> getIdpacientes_p() {
        return idpacientes_p;
    }

    public void setIdpacientes_p(List<Long> idpacientes_p) {
        this.idpacientes_p = idpacientes_p;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getMensaje_p() {
        return mensaje_p;
    }

    public void setMensaje_p(String mensaje_p) {
        this.mensaje_p = mensaje_p;
    }

 

    
  
    
}
