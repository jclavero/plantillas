/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.azurian.azurianfitx.mensajeriaPush;

import com.turo.pushy.apns.ApnsClient;
import com.turo.pushy.apns.ApnsClientBuilder;
import com.turo.pushy.apns.auth.ApnsSigningKey;
import com.turo.pushy.apns.util.ApnsPayloadBuilder;
import com.turo.pushy.apns.util.SimpleApnsPushNotification;
import com.turo.pushy.apns.util.TokenUtil;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jclavero
 */
public class PushIOS {

    public PushIOS() {

    }

    public void enviar(String tokens, String mensaje) {
        try {
            final ApnsClient apnsClient = new ApnsClientBuilder()
                    .setApnsServer(ApnsClientBuilder.DEVELOPMENT_APNS_HOST)
                    .setSigningKey(ApnsSigningKey.loadFromPkcs8File(new File("/home/ec2-user/AuthKey_XG8T92CPX3.p8"),
                            "LYCH9RZA2A", "XG8T92CPX3"))
                    .build();

            final ApnsPayloadBuilder payloadBuilder = new ApnsPayloadBuilder();
            payloadBuilder.setAlertBody(mensaje);

            final String payload = payloadBuilder.buildWithDefaultMaximumLength();
            final String token = TokenUtil.sanitizeTokenString(tokens);
            
            //pakage del proyecto
            SimpleApnsPushNotification pushNotification = new SimpleApnsPushNotification(token, "com.test.test.test", payload);
            apnsClient.sendNotification(pushNotification);

        } catch (Exception ex) {
            Logger.getLogger(PushIOS.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
