package cl.azurian.azurianfitx.mensajeriaPush;

import cl.azurian.azurianfitx.restservices.AdmPasswordRestFacade;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author jclavero
 */
public class PushAndroid {

    public final static String AUTH_KEY_FCM = "AAAA15jCCQc:APA91bG_0jwJxpXsy2ke-4wynJ6HtXE3QvRpSC7q4NAWGl3tFasGrA2oA-R4VhfMmQXdnK4IXqNGeabzPYx_pPQLuMmvbNmThlPCtrsfpXIOL-fJtWzW3RDMQihIV2HmjkdTaSYHXXR-";

    public final static String API_URL_FCM = "https://fcm.googleapis.com/fcm/send";

    public void pushFCMNotification(String userDeviceIdKey, String title, String message) throws Exception {

        try {
            String authKey = AUTH_KEY_FCM;   // You FCM AUTH key
            String FMCurl = API_URL_FCM;

            URL url = new URL(FMCurl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);

            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "key=" + authKey);
            conn.setRequestProperty("Content-Type", "application/json");

            JSONObject json = new JSONObject();
            json.put("to", userDeviceIdKey.trim());
            JSONObject info = new JSONObject();
            info.put("title", title); // Notification title
            info.put("body", message); // Notification body
            json.put("notification", info);
            System.out.println(json.toString());

            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(json.toString());
            wr.flush();
            wr.close();
            System.out.println(conn.getResponseMessage());
            conn.getInputStream();
        } catch (IOException | JSONException ex) {
            Logger.getLogger(AdmPasswordRestFacade.class.getName()).log(Level.SEVERE, null, ex);   

        }

    }
}
