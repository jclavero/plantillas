/*
 * request filtro
 */
package cl.azurian.azurianfitx.filtro;

import cl.azurian.azurianfitx.errorHandler.ErrorHandler;
import cl.azurian.azurianfitx.verificacion.Utils;
import cl.azurian.azurianfitx.verificacion.VerifyService;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.AuthenticationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author jclavero
 */
@Provider
@PreMatching
public class Filtro implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        
        
        System.out.println("Filtro " + requestContext.getUriInfo().getRequestUri().toString());
        VerifyService verify = new VerifyService();
        ErrorHandler error = new ErrorHandler();
        Utils util = new Utils();

        String llamado = requestContext.getUriInfo().getRequestUri().toString();
 
        if (requestContext.getMethod().equalsIgnoreCase("OPTIONS")) {
          

        } else if(llamado.contains("password/reset")){
        
        
        
        }
        else {
        try {
            error.setMessageError("Error");
            error.setErrorCode(400);
            String auth = requestContext.getHeaderString("Authorization");
            String[] authComponents = auth.split(" ");
            String type = authComponents[0];
            String value = authComponents[1];
            System.out.println("Authorization " + type + " " + value);

            String jsonSecurityMedico = value;
            byte[] decryp = verify.decryptAES1(jsonSecurityMedico);
            byte[] jsonMedico = util.cortadorBloquesMedico(decryp);
            String responseJson = util.cortadorBloquesMedicoResponse(decryp);
            byte[] bloque = new byte[32];
            System.arraycopy(decryp, decryp.length - 32, bloque, 0, bloque.length);
            String expires = new String(bloque, StandardCharsets.ISO_8859_1);
            expires = expires.trim();
            JSONObject json = new JSONObject(responseJson);
            boolean admin = json.getBoolean("admin");

            if (verify.controladorOperacion(jsonMedico, jsonSecurityMedico)) {

                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(Long.parseLong(expires));
                if (calendar.after(new Date())) {

                    throw new AuthenticationException("Security Exception");
                }

                if (llamado.contains("admin")) {
             
                
                }else{
                // no contiene admin en path
                if(llamado.contains("webresources/services.grupo/listar")){
                    
                        if (admin == false) {
                        //throw new AuthenticationException("No Admin");
                        requestContext.setRequestUri(new URI(requestContext.getUriInfo().getRequestUri() + "&admin="+"noadmin"));
                    }else{
                     requestContext.setRequestUri(new URI(requestContext.getUriInfo().getRequestUri() + "&admin="+"admin"));
                    }
                   
                    
                }
                
                }

            } else {

                throw new AuthenticationException("Security Exception");
            }

        } catch (NumberFormatException | AuthenticationException | JSONException ex) {

            Response.status(Response.Status.BAD_REQUEST).entity(ex).build();
        } catch (URISyntaxException ex) {
            Logger.getLogger(Filtro.class.getName()).log(Level.SEVERE, null, ex);
        }

  }
    }

}
