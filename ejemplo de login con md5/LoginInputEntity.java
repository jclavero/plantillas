/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.azurian.zauthmedicos;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



/**
 *
 * @author jclavero
 */

@Entity
public class LoginInputEntity implements Serializable{

    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String rut_p;
    private String dv_p;
    private String password_p;

   
    public String getRut_p() {
        return rut_p;
    }

    public void setRut_p(String rut_p) {
        this.rut_p = rut_p;
    }

    public String getDv_p() {
        return dv_p;
    }

    public void setDv_p(String dv_p) {
        this.dv_p = dv_p;
    }

    public String getPassword_p() {
        return password_p;
    }

    public void setPassword_p(String password_p) {
        this.password_p = password_p;
    }

    
    
    
    
}
