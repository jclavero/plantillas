/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.azurian.zauthmedicos;


import com.azurian.zauthmedicos.customEntity.LoginResponse;
import com.azurian.zauthmedicos.customEntity.SecurityInfoMedico;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author jclavero
 */
@Entity
public class LoginOutputEntity implements Serializable {
  private static final long serialVersionUID = 1L;
  @Id
  private Long id;	
  private LoginResponse infoMedico ;
  private SecurityInfoMedico infoSecurityMedico;

    public LoginOutputEntity() {
    }

    public LoginOutputEntity(Long id, LoginResponse infoMedico, SecurityInfoMedico infoSecurityMedico) {
        this.id = id;
        this.infoMedico = infoMedico;
        this.infoSecurityMedico = infoSecurityMedico;
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LoginResponse getInfoMedico() {
        return infoMedico;
    }

    public void setInfoMedico(LoginResponse infoMedico) {
        this.infoMedico = infoMedico;
    }



    public SecurityInfoMedico getInfoSecurityMedico() {
        return infoSecurityMedico;
    }

    public void setInfoSecurityMedico(SecurityInfoMedico infoSecurityMedico) {
        this.infoSecurityMedico = infoSecurityMedico;
    }

  

   

}
