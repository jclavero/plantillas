package com.azurian.zauthmedicos.controller;

import com.azurian.zauthmedicos.customEntity.LoginResponse;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class ServicioLogin {

    private static MessageDigest digester = null;

    // principal
    public String getEncoderJson(LoginResponse loginResponse, long exp) {

        String key = "Hola1234Hola1234";

        byte[] value = getBytesToJson(loginResponse);
        String md5 = getMD5(Arrays.toString(value));
        byte[] valueMd5 = getBytesToJsonMD5(loginResponse, md5, exp);
        String response = encryptAES2(valueMd5, key);
        return response;
    }

    public String getMD5(String str) {
        try {
            digester = MessageDigest.getInstance("MD5");
            final byte[] messageDigest = digester.digest(str.getBytes());
            final BigInteger number = new BigInteger(1, messageDigest);
            return String.format("%032x", number);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String encryptAES2(byte[] value, String key) {
        try {
            SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("UTF-8"), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] encrypted = cipher.doFinal(value);
            return Base64.getEncoder().encodeToString(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private byte[] metodoaddbytes(byte[] response, byte[] value, int inicio) {
        System.arraycopy(value, 0, response, inicio, value.length);
        return response;
    }
 
    public static final byte[] IntToByteArray(long data) {

        long idDesdeBD = (long)data;
        //indice 0 -> LSB

        byte[] bytes = {0x00, 0x00, 0x00, 0x00, 0x00};

        bytes[0] = (byte) (idDesdeBD);
        bytes[1] = (byte) (idDesdeBD >> 8);
        bytes[2] = (byte) (idDesdeBD >> 16);
        bytes[3] = (byte) (idDesdeBD >> 24);
        bytes[4] = (byte) (idDesdeBD >> 32);

        System.out.println(String.format("byte[0] = %x", bytes[0]));
        System.out.println(String.format("byte[1] = %x", bytes[1]));
        System.out.println(String.format("byte[2] = %x", bytes[2]));
        System.out.println(String.format("byte[3] = %x", bytes[3]));
        System.out.println(String.format("byte[4] = %x", bytes[4]));

        return bytes;
    }

    private byte[] getBytesToJson(LoginResponse loginResponse) {
        Integer aux1 = loginResponse.getIdmedico();
        long aux2 = aux1.longValue();
       byte[] aux3 = IntToByteArray(aux2);
       
       
        ByteBuffer bb = ByteBuffer.wrap(aux3);
        ByteBuffer test = bb.get(aux3);
      byte[] aux4 ={};
        
       
        byte[] response = new byte[352];
        Arrays.fill(response, (byte) 0);
        response = metodoaddbytes(response, loginResponse.getMednombre().getBytes(), 0);
        
//        response[32] = aux3[0];
//        response[33] = aux3[1];
//        response[34] = aux3[2];
//        response[35] = aux3[3];

        response = metodoaddbytes(response, test.array(), 32);

        response = metodoaddbytes(response, loginResponse.getMedapellidopaterno().getBytes(), 64);
        response = metodoaddbytes(response, loginResponse.getMedapellidomaterno().getBytes(), 96);
        response = metodoaddbytes(response, loginResponse.getMedrutcolelgiomedico().getBytes(), 128);
        response = metodoaddbytes(response, loginResponse.getMedemail().getBytes(), 160);
        response = metodoaddbytes(response, loginResponse.getMedespecialidad().getBytes(), 192);
        response = metodoaddbytes(response, loginResponse.getRutmedico().getBytes(), 224);
        response = metodoaddbytes(response, String.valueOf(loginResponse.getEstado()).getBytes(), 256);
        response = metodoaddbytes(response, loginResponse.getDigitoverificador().getBytes(), 288);
        response = metodoaddbytes(response, String.valueOf(loginResponse.isAdmin()).getBytes(), 320);
        System.out.println(Arrays.toString(response));
        return response;

    }

    private byte[] getBytesToJsonMD5(LoginResponse loginResponse, String md5, long exp) {

        Integer aux1 = loginResponse.getIdmedico();
        long aux2 = aux1.longValue();
          byte[] aux4 ={};
          
        byte[] aux3 = IntToByteArray(aux2);
           ByteBuffer bb = ByteBuffer.wrap(aux3);
        ByteBuffer test = bb.get(aux3);
        
        
        byte[] response = new byte[416];
        Arrays.fill(response, (byte) 0);
        response = metodoaddbytes(response, md5.getBytes(), 0);
        response = metodoaddbytes(response, loginResponse.getMednombre().getBytes(), 32);
        
//        response[64] = aux3[0];
//        response[65] = aux3[1];
//        response[66] = aux3[2];
//        response[67] = aux3[3];
        response = metodoaddbytes(response, test.array(), 64);

        response = metodoaddbytes(response, loginResponse.getMedapellidopaterno().getBytes(), 96);
        response = metodoaddbytes(response, loginResponse.getMedapellidomaterno().getBytes(), 128);
        response = metodoaddbytes(response, loginResponse.getMedrutcolelgiomedico().getBytes(), 160);
        response = metodoaddbytes(response, loginResponse.getMedemail().getBytes(), 192);
        response = metodoaddbytes(response, loginResponse.getMedespecialidad().getBytes(), 224);
        response = metodoaddbytes(response, loginResponse.getRutmedico().getBytes(), 256);
        response = metodoaddbytes(response, String.valueOf(loginResponse.getEstado()).getBytes(), 288);
        response = metodoaddbytes(response, loginResponse.getDigitoverificador().getBytes(), 320);
        response = metodoaddbytes(response, String.valueOf(loginResponse.isAdmin()).getBytes(), 352);
        response = metodoaddbytes(response, String.valueOf(exp).getBytes(), 384);
        return response;

    }

    public long getExpiracion(int tipo, int addAnios) {
        Calendar c = Calendar.getInstance();
        c.getTime();
        long mili1 = c.getTimeInMillis();
        c.add(tipo, addAnios);
        long mili2 = c.getTimeInMillis();
        long miliFinal = mili2 - mili1;
        return miliFinal;
    }

}
