/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.azurian.zauthmedicos;

import com.azurian.zauthmedicos.controller.ServicioLogin;
import com.azurian.zauthmedicos.customEntity.LoginResponse;
import com.azurian.zauthmedicos.customEntity.SecurityInfoMedico;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Calendar;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author jclavero
 */
@Stateless
@Path("login")
public class LoginRest {

    @PersistenceContext(unitName = "con.codefit3")
    private EntityManager em;

    ServicioLogin servicioLogin = new ServicioLogin();
    LoginOutputEntity output = new LoginOutputEntity();
    ErrorHandler error = new ErrorHandler();

    @GET
    public LoginOutputEntity test() {

        LoginOutputEntity entity = new LoginOutputEntity();

        return entity;

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response doLogin(LoginInputEntity entity) throws Exception {

        try {
            String passMedico =servicioLogin.getMD5(entity.getPassword_p());
            StoredProcedureQuery query = em.createNamedStoredProcedureQuery("oauthlogin");
            query.setParameter("rut_p", entity.getRut_p());
            query.setParameter("dv_p", entity.getDv_p());
            query.setParameter("password_p", passMedico);
            LoginResponse user = (LoginResponse) query.getSingleResult();
            // output.setInfoMedico(user);
            servicioLogin = new ServicioLogin();
            long exp = servicioLogin.getExpiracion(Calendar.YEAR, 2);
            ObjectMapper mapper = new ObjectMapper();
            String jsonMedico = mapper.writeValueAsString(user);

            System.out.println(jsonMedico);
            String key = servicioLogin.getEncoderJson(user, exp);

            SecurityInfoMedico infoSecurityMedico = new SecurityInfoMedico(exp, key);

            output.setInfoSecurityMedico(infoSecurityMedico);

        } catch (Exception ex) {
            error.setMessageError("Rut o Password erroneo");
            error.setErrorCode(400);
            return Response.status(Response.Status.BAD_REQUEST).entity(error).build();
        }
        return Response.accepted(output).build();
    }

}
