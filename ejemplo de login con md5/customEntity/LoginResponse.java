/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.azurian.zauthmedicos.customEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jclavero
 */
@Entity
public class LoginResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "mednombre")
    private String mednombre;
    @Basic(optional = false)
    @Column(name = "idmedico")
    private Integer idmedico;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "medapellidopaterno")
    private String medapellidopaterno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "medapellidomaterno")
    private String medapellidomaterno;
    @Size(max = 50)
    @Column(name = "medrutcolelgiomedico")
    private String medrutcolelgiomedico;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "medemail")
    private String medemail;
    @Size(max = 100)
    @Column(name = "medespecialidad")
    private String medespecialidad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rutmedico")
    private String rutmedico;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private boolean estado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "digitoverificador")
    private String digitoverificador;

    @Column(name = "admin")
    private boolean admin;

    public Integer getIdmedico() {
        return idmedico;
    }

    public void setIdmedico(Integer idmedico) {
        this.idmedico = idmedico;
    }

    public String getMednombre() {
        return mednombre;
    }

    public void setMednombre(String mednombre) {
        this.mednombre = mednombre;
    }

    public String getMedapellidopaterno() {
        return medapellidopaterno;
    }

    public void setMedapellidopaterno(String medapellidopaterno) {
        this.medapellidopaterno = medapellidopaterno;
    }

    public String getMedapellidomaterno() {
        return medapellidomaterno;
    }

    public void setMedapellidomaterno(String medapellidomaterno) {
        this.medapellidomaterno = medapellidomaterno;
    }

    public String getMedrutcolelgiomedico() {
        return medrutcolelgiomedico;
    }

    public void setMedrutcolelgiomedico(String medrutcolelgiomedico) {
        this.medrutcolelgiomedico = medrutcolelgiomedico;
    }

    public String getMedemail() {
        return medemail;
    }

    public void setMedemail(String medemail) {
        this.medemail = medemail;
    }

    public String getMedespecialidad() {
        return medespecialidad;
    }

    public void setMedespecialidad(String medespecialidad) {
        this.medespecialidad = medespecialidad;
    }

    public String getRutmedico() {
        return rutmedico;
    }

    public void setRutmedico(String rutmedico) {
        this.rutmedico = rutmedico;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getDigitoverificador() {
        return digitoverificador;
    }

    public void setDigitoverificador(String digitoverificador) {
        this.digitoverificador = digitoverificador;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

}
