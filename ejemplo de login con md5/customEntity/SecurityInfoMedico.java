/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.azurian.zauthmedicos.customEntity;

import java.io.Serializable;

/**
 *
 * @author jclavero
 */
public class SecurityInfoMedico implements Serializable{
private static final long serialVersionUID = 4L;
    public SecurityInfoMedico() {
    }
    
    private Long expiresAt ;
    private String key;

    public SecurityInfoMedico(Long expiresAt, String key) {
        this.expiresAt = expiresAt;
        this.key = key;
    }

    
    
    public Long getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Long expiresAt) {
        this.expiresAt = expiresAt;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
    
   
    
    
}
