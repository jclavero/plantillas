/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.azurian.zauthmedicos.customEntity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureParameter;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jclavero
 */
@Entity
@NamedStoredProcedureQueries({
		@NamedStoredProcedureQuery(name = "oauthlogin", procedureName = "oauthlogin", resultClasses = {
				LoginResponse.class }, parameters = {
						@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "rut_p"),
						@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "dv_p"),
						@StoredProcedureParameter(mode = ParameterMode.IN, type = String.class, name = "password_p")

		}) })
@Table(name = "medico", catalog = "codefitv3", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Medico.findAll", query = "SELECT m FROM Medico m"),
    @NamedQuery(name = "Medico.findByIdmedico", query = "SELECT m FROM Medico m WHERE m.idmedico = :idmedico"),
    @NamedQuery(name = "Medico.findByMednombre", query = "SELECT m FROM Medico m WHERE m.mednombre = :mednombre"),
    @NamedQuery(name = "Medico.findByMedapellidopaterno", query = "SELECT m FROM Medico m WHERE m.medapellidopaterno = :medapellidopaterno"),
    @NamedQuery(name = "Medico.findByMedapellidomaterno", query = "SELECT m FROM Medico m WHERE m.medapellidomaterno = :medapellidomaterno"),
    @NamedQuery(name = "Medico.findByMedrutcolelgiomedico", query = "SELECT m FROM Medico m WHERE m.medrutcolelgiomedico = :medrutcolelgiomedico"),
    @NamedQuery(name = "Medico.findByMedemail", query = "SELECT m FROM Medico m WHERE m.medemail = :medemail"),
    @NamedQuery(name = "Medico.findByMedespecialidad", query = "SELECT m FROM Medico m WHERE m.medespecialidad = :medespecialidad"),
    @NamedQuery(name = "Medico.findByRutmedico", query = "SELECT m FROM Medico m WHERE m.rutmedico = :rutmedico"),
    @NamedQuery(name = "Medico.findByEstado", query = "SELECT m FROM Medico m WHERE m.estado = :estado"),
    @NamedQuery(name = "Medico.findByDigitoverificador", query = "SELECT m FROM Medico m WHERE m.digitoverificador = :digitoverificador"),
    @NamedQuery(name = "Medico.findByContrasena", query = "SELECT m FROM Medico m WHERE m.contrasena = :contrasena")})
public class Medico implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idmedico")
    private Integer idmedico;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "mednombre")
    private String mednombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "medapellidopaterno")
    private String medapellidopaterno;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "medapellidomaterno")
    private String medapellidomaterno;
    @Size(max = 50)
    @Column(name = "medrutcolelgiomedico")
    private String medrutcolelgiomedico;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "medemail")
    private String medemail;
    @Size(max = 100)
    @Column(name = "medespecialidad")
    private String medespecialidad;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rutmedico")
    private String rutmedico;
    @Basic(optional = false)
    @NotNull
    @Column(name = "estado")
    private boolean estado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "digitoverificador")
    private String digitoverificador;
    @Size(max = 2147483647)
    @Column(name = "contrasena")
    private String contrasena;
    @JoinColumn(name = "idperfil", referencedColumnName = "idperfil")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Perfil idperfil;

    public Medico() {
    }

    public Medico(Integer idmedico) {
        this.idmedico = idmedico;
    }

    public Medico(Integer idmedico, String mednombre, String medapellidopaterno, String medapellidomaterno, String medemail, String rutmedico, boolean estado, String digitoverificador) {
        this.idmedico = idmedico;
        this.mednombre = mednombre;
        this.medapellidopaterno = medapellidopaterno;
        this.medapellidomaterno = medapellidomaterno;
        this.medemail = medemail;
        this.rutmedico = rutmedico;
        this.estado = estado;
        this.digitoverificador = digitoverificador;
    }

    public Integer getIdmedico() {
        return idmedico;
    }

    public void setIdmedico(Integer idmedico) {
        this.idmedico = idmedico;
    }

    public String getMednombre() {
        return mednombre;
    }

    public void setMednombre(String mednombre) {
        this.mednombre = mednombre;
    }

    public String getMedapellidopaterno() {
        return medapellidopaterno;
    }

    public void setMedapellidopaterno(String medapellidopaterno) {
        this.medapellidopaterno = medapellidopaterno;
    }

    public String getMedapellidomaterno() {
        return medapellidomaterno;
    }

    public void setMedapellidomaterno(String medapellidomaterno) {
        this.medapellidomaterno = medapellidomaterno;
    }

    public String getMedrutcolelgiomedico() {
        return medrutcolelgiomedico;
    }

    public void setMedrutcolelgiomedico(String medrutcolelgiomedico) {
        this.medrutcolelgiomedico = medrutcolelgiomedico;
    }

    public String getMedemail() {
        return medemail;
    }

    public void setMedemail(String medemail) {
        this.medemail = medemail;
    }

    public String getMedespecialidad() {
        return medespecialidad;
    }

    public void setMedespecialidad(String medespecialidad) {
        this.medespecialidad = medespecialidad;
    }

    public String getRutmedico() {
        return rutmedico;
    }

    public void setRutmedico(String rutmedico) {
        this.rutmedico = rutmedico;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getDigitoverificador() {
        return digitoverificador;
    }

    public void setDigitoverificador(String digitoverificador) {
        this.digitoverificador = digitoverificador;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public Perfil getIdperfil() {
        return idperfil;
    }

    public void setIdperfil(Perfil idperfil) {
        this.idperfil = idperfil;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmedico != null ? idmedico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Medico)) {
            return false;
        }
        Medico other = (Medico) object;
        if ((this.idmedico == null && other.idmedico != null) || (this.idmedico != null && !this.idmedico.equals(other.idmedico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.azurian.zauthmedicos.customEntity.Medico[ idmedico=" + idmedico + " ]";
    }
    
}
